#!/usr/bin/env bash
# Generate the binding
bindgen wrapper.h -o lib.rs -- -I target/debug/build/marpa-sys-*/out/include/
read -r -d '' ALLOWS << EOF
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

EOF

echo "$ALLOWS" | cat - lib.rs > tempfile && mv tempfile lib.rs
