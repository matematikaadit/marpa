extern crate tempdir;
extern crate bindgen;

use std::env;
use std::path::Path;
use std::process::Command;
use std::ffi::OsStr;
use tempdir::TempDir;

fn main() {
    let prefix = env::var_os("OUT_DIR").expect("OUT_DIR undefined");
    let prefix = Path::new(prefix.as_os_str());

    make(&prefix);
    // bindgen(&prefix);
    // TODO: decide whether to use bindgen here or offline
}

fn make(prefix: &Path) {
    let configure_opts = [
        OsStr::new("--disable-shared"), // we only want the static lib
        OsStr::new("--prefix"), prefix.as_os_str(), // make install target
        OsStr::new("--with-pic")
    ];

    let vendor = env::var_os("CARGO_MANIFEST_DIR").expect("CARGO_MANIFEST_DIR undefined");
    let vendor = Path::new(&vendor).join("libmarpa-8.6.0");

    // prepping tempdir for building the project
    let tempdir = TempDir::new_in(prefix, "build").expect("Can't create tempdir");

    let execute = |cmd: &mut Command| {
        println!("running: {:?}", cmd);

        let status = cmd
            .current_dir(tempdir.path()) // build in tempdir
            .status()
            .unwrap()
            .success();

        assert!(status);
    };

    // configure
    execute(Command::new(vendor.join("configure")).args(&configure_opts));
    // make
    execute(Command::new("make").arg("-j4"));
    // make install
    execute(Command::new("make").arg("install"));

    let prefix = prefix.display();
    println!("cargo:rustc-link-lib=static=marpa");
    println!("cargo:rustc-link-search=native={}/lib", prefix);
    println!("cargo:root={}", prefix);
    println!("cargo:libdir={}/lib", prefix);
    println!("cargo:include={}/include", prefix);
}
